import os

from conans import ConanFile, CMake

project_name = "gfx"


class GfxConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Gfx abstraction library"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.container/0.0.1@asd/testing",
        "asd.math/0.0.1@asd/testing",
        "asd.color/0.0.1@asd/testing"
    )

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy("*.h", dst="include", src="include")

    def package_info(self):
        self.info.header_only()
