//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/registry.h>
#include <gfx/shader.h>
#include <gfx/uniform.h>

#include <container/set.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        enum class polygon_mode
        {
            fill,
            wireframe
        };

        template <class Gfx>
        class pipeline
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using graphics_type = Gfx;

            void activate();
        };

        template <class Gfx>
        struct init_data<pipeline<Gfx>>
        {
            init_data(gfx::shader_program<Gfx> & program, gfx::polygon_mode polygon_mode = gfx::polygon_mode::fill) :
                program(program), polygon_mode(polygon_mode) {}

            gfx::shader_program<Gfx> & program;
            gfx::polygon_mode polygon_mode;
        };

        template <class Gfx>
        using pipeline_data = init_data<pipeline<Gfx>>;

        template <class Gfx>
        using pipeline_registry = registry<pipeline<Gfx>>;
    }
}
