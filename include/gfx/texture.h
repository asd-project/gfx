//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/span.h>
#include <math/rect.h>

#include <gfx/registry.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        enum class texture_format
        {
            grayscale,
            grayscale_alpha,
            rgb,
            rgba,
            bgra,
            rgb16f,
            rgba16f,
            rgb32f,
            rgba32f
        };

        enum class texture_filtering : u8
        {
            nearest,
            linear
        };

        template <class Gfx>
        class texture;

        using pixel_data = span<const void>;

        template <class Gfx>
        struct init_data<texture<Gfx>>
        {
            pixel_data pixels = nullptr;
            texture_format format = texture_format::rgba;
            math::uint_size size = {1, 1};
            int internal_format = 0;
            texture_filtering filtering = texture_filtering::nearest;
        };

        template <class Gfx>
        using texture_data = init_data<texture<Gfx>>;

        constexpr u8 bytes_per_pixel(texture_format format) {
            switch (format) {
                case texture_format::grayscale:
                    return 1;

                case texture_format::grayscale_alpha:
                    return 2;

                case texture_format::rgb:
                    return 3;

                case texture_format::rgba:
                case texture_format::bgra:
                    return 4;

                case texture_format::rgb16f:
                    return 6;

                case texture_format::rgba16f:
                    return 8;

                case texture_format::rgb32f:
                    return 12;

                case texture_format::rgba32f:
                    return 16;
            }

            BOOST_ASSERT_MSG(false, "[gfx][bytes_per_pixel] Invalid texture format");
            return 0;
        }

        enum class section_snapping : u8
        {
            start,
            center,
            end
        };

        using area_snapping = math::point<section_snapping>;

        template <class Gfx>
        class texture
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using graphics_type = Gfx;

            math::uint_size size() const = delete;
            size_t size_in_bytes() const = delete;
            texture_format format() const = delete;
            u8 bpp() const = delete;

            void bind(int slot) = delete;
            void update(const texture_data<Gfx> & data) = delete;
            void update_pixels(pixel_data data) = delete;
            void update_region(const math::uint_rect & region, pixel_data data) = delete;

            std::vector<std::byte> read_pixels() = delete;
            void set_filtering(texture_filtering filtering) = delete;
            void resize(const math::uint_size & size, area_snapping snapping = {section_snapping::start, section_snapping::start}) = delete;
        };

        template <class Gfx>
        using texture_registry = registry<texture<Gfx>>;

        inline u32 snapping_offset(section_snapping snapping, u32 delta) {
            switch (snapping) {
                case section_snapping::start:
                    return 0;
                case section_snapping::center:
                    return delta / 2;
                case section_snapping::end:
                    return delta;
            }

            BOOST_ASSERT_MSG(false, "[gfx][snapping_offset] Invalid snapping offset");
            return 0;
        }

        namespace detail
        {
            inline void copy_pixels(span<std::byte> pixels, span<std::byte> destination_pixels, const math::uint_rect & region, const math::uint_rect & destination_region, const math::uint_size & area_size, u8 bpp) {
                auto old_row_size = region.width() * bpp;
                auto new_row_size = destination_region.width() * bpp;
                auto row_size = area_size.x * bpp;

                for (u32 y = 0; y < area_size.y; ++y) {
                    auto begin = pixels.begin() + (region.top + y) * old_row_size + region.left;
                    std::copy(begin, begin + row_size, destination_pixels.begin() + (destination_region.top + y) * new_row_size + destination_region.left);
                }
            }
        }

        inline void copy_pixels(span<std::byte> pixels, span<std::byte> destination_pixels, const math::uint_rect & region, const math::uint_rect & destination_region, u8 bpp) {
            auto area_size = math::min(region.size(), destination_region.size());
            gfx::detail::copy_pixels(destination_pixels, pixels, region, destination_region, area_size, bpp);
        }

        inline std::vector<std::byte> copy_pixels(span<std::byte> pixels, const math::uint_rect & region, const math::uint_rect & destination_region, u8 bpp) {
            std::vector<std::byte> destination_pixels{static_cast<size_t>(destination_region.area()) * bpp, std::byte(0)};
            copy_pixels(destination_pixels, pixels, region, destination_region, bpp);

            return destination_pixels;
        }

        inline void copy_pixels(span<std::byte> pixels, span<std::byte> destination_pixels, const math::uint_size & size, const math::uint_size & new_size, u8 bpp, area_snapping snapping = {section_snapping::start, section_snapping::start}) {
            auto area_size = math::min(size, new_size);
            auto src_delta = snapping.map(snapping_offset, size - area_size);
            auto dst_delta = snapping.map(snapping_offset, new_size - area_size);

            gfx::detail::copy_pixels(destination_pixels, pixels, {src_delta, size}, {dst_delta, new_size}, area_size, bpp);
        }

        inline std::vector<std::byte> copy_pixels(span<std::byte> pixels, const math::uint_size & size, const math::uint_size & new_size, u8 bpp, area_snapping snapping = {section_snapping::start, section_snapping::start}) {
            std::vector<std::byte> destination_pixels{static_cast<size_t>(new_size.area()) * bpp, std::byte(0)};
            copy_pixels(destination_pixels, pixels, size, new_size, bpp);

            return destination_pixels;
        }
    }
}
