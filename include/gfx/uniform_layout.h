//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/span.h>
#include <gfx/buffer.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        struct uniform_layout_element
        {
            constexpr uniform_layout_element(std::string_view name, int32 offset, ctti::type_index type) :
                name(name), offset(offset), type(type) {}

            constexpr bool operator == (const uniform_layout_element & e) const {
                return offset == e.offset && type == e.type && name == e.name;
            }

            constexpr bool operator != (const uniform_layout_element & e) const {
                return offset != e.offset || type != e.type || name != e.name;
            }

            std::string_view name;
            int32 offset;
            ctti::type_index type;
        };

        struct uniform_layout
        {
            constexpr uniform_layout(std::string_view name, int32 size = 0) :
                name(name),
                size(size)
            {}

            bool operator == (const uniform_layout & l) const noexcept {
                return this == &l || (name == l.name && size == l.size && elements_data() == l.elements_data());
            }

            bool operator != (const uniform_layout & l) const noexcept {
                return this != &l && (name != l.name || size != l.size || elements_data() != l.elements_data());
            }

            const uniform_layout_element & at(size_t index) const {
                if (index >= elements_data().size()) {
                    BOOST_THROW_EXCEPTION(std::out_of_range("Index is out of bounds"));
                }

                return operator[](index);
            }

            const uniform_layout_element & operator[](size_t index) const {
                return elements_data()[index];
            }

            size_t elements_count() const noexcept {
                return elements_data().size();
            }

            virtual span<const uniform_layout_element> elements_data() const noexcept = 0;

            std::string_view name;
            int32 size;
        };

        namespace uniforms
        {
            template <class T, class ... E, size_t ... Indices, useif<(sizeof ... (Indices) > 0)>>
            constexpr T sum_sizes(std::index_sequence<Indices...>) {
                return static_cast<T>((std::tuple_element_t<Indices, std::tuple<E...>>::size + ...));
            }

            template <class T, class ... E>
            constexpr T sum_sizes(std::index_sequence<>) {
                return 0;
            }

            template <class T>
            struct element
            {
                using type = T;
                static constexpr int32 size = sizeof(T);

                constexpr uniform_layout_element create(int32 offset) const {
                    return { name, offset, ctti::unnamed_type_id<raw_data_t<T>>() };
                }

                std::string_view name;
            };

            namespace detail
            {
                template <size_t Index, class ... Elements>
                constexpr auto create_element(const std::tuple<Elements...> & elements) {
                    return get<Index>(elements).create(sum_sizes<int32, Elements...>(std::make_index_sequence<Index>{}));
                }

                template <class ... Elements, size_t ... Indices>
                constexpr auto transform_tuple(const std::tuple<Elements...> & elements, std::index_sequence<Indices...>) {
                    constexpr size_t N = sizeof ... (Elements);
                    return std::array<uniform_layout_element, N>{{ create_element<Indices>(elements)... }};
                }
            }
        }

        template <class ... Elements>
        struct static_uniform_layout_storage : uniform_layout
        {
            static constexpr size_t N = sizeof ... (Elements);

            constexpr static_uniform_layout_storage(std::string_view name, int32 size, std::array<uniform_layout_element, N> && elements) :
                uniform_layout(name, size),
                elements(std::move(elements))
            {}

            virtual span<const uniform_layout_element> elements_data() const noexcept override final {
                return elements;
            }

            std::array<uniform_layout_element, N> elements;
        };

        struct dynamic_uniform_layout_storage : uniform_layout
        {
            dynamic_uniform_layout_storage(std::string_view name) :
                uniform_layout(name)
            {}

            virtual span<const uniform_layout_element> elements_data() const noexcept override final {
                return elements;
            }

            template <class T>
            void push_back(const uniforms::element<T> & e) {
                elements.push_back(e.create(size));
                size += uniforms::element<T>::size;
            }

            array_list<uniform_layout_element> elements;
        };

        template <class ... Elements>
        constexpr auto create_uniform_layout(std::string_view name, const Elements & ... elements) {
            using indices_t = std::make_index_sequence<sizeof ... (Elements)>;
            return static_uniform_layout_storage<typename Elements::type...>(name, uniforms::sum_sizes<int32, Elements...>(indices_t{}), uniforms::detail::transform_tuple(std::make_tuple(elements...), indices_t{}));
        }

        namespace uniforms
        {
            static constexpr auto Color = create_uniform_layout("Color", element<color::linear_rgb>{"color"});
            static constexpr auto Model = create_uniform_layout("Model", element<math::float_matrix>{"model"});
            static constexpr auto View = create_uniform_layout("View", element<math::float_matrix>{"view"});
            static constexpr auto Projection = create_uniform_layout("Projection", element<math::float_matrix>{"projection"});
        }
    }
}
