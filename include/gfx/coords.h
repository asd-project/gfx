//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/rect.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        using number = float;
        using coord = number;
        using area = math::rect<number>;
        using position = typename area::point_type;
        using size = typename area::size_type;
        using dimension = typename size::coord_type;
        using range = typename area::range_type;
        using side = math::side;
    }
}
