//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <concepts>

#include <meta/common.h>
#include <meta/id.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        enum class matrix_order
        {
            row_major,
            column_major
        };

        template <class T>
        concept graphics = requires {
            { T::matrix_order };
        };

        template <class T>
        concept object = requires {
            gfx::graphics<typename T::graphics_type>;
        };

        template<class Tag>
        using object_id = meta::id<uint32, Tag, std::integral_constant<uint32, uint32(-1)>>;
    }
}
