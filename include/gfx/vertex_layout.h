//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <array>

#include <container/span.h>
#include <algorithm/string.h>
#include <gfx/buffer.h>

#include <meta/types/slice.h>
#include <meta/types/nth.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class A>
        concept aliased_attribute = requires { { A::alias }; };

        struct vertex_attribute
        {
            const char * name = "unknown";
            const char * alias = nullptr;
            ctti::type_index type = ctti::unnamed_type_id<void>();
            int32 size_in_bytes = 0;
            int32 units = 0;
            int32 offset = 0;
            bool normalize = false;

            constexpr bool operator == (const vertex_attribute & e) const {
                return this == &e || (units == e.units && type == e.type && lexicographical_compare(name, e.name) == 0);
            }

            constexpr bool operator != (const vertex_attribute & e) const {
                return this != &e && (units != e.units || type != e.type || lexicographical_compare(name, e.name) != 0);
            }

            template <class A>
                requires (!aliased_attribute<A>)
            static constexpr vertex_attribute create(int32 offset) {
                return { A::name, nullptr, ctti::unnamed_type_id<typename A::type>(), A::size_in_bytes, A::units, offset, A::normalize };
            }

            template <class A>
                requires (aliased_attribute<A>)
            static constexpr vertex_attribute create(int32 offset) {
                return { A::name, A::alias, ctti::unnamed_type_id<typename A::type>(), A::size_in_bytes, A::units, offset, A::normalize };
            }
        };

        class vertex_layout
        {
        public:
            constexpr vertex_layout(span<const vertex_attribute> attributes = {}, int32 size_in_bytes = 0) noexcept :
                _attributes(attributes),
                _size_in_bytes(size_in_bytes)
            {}

            constexpr vertex_layout(const vertex_attribute & attribute) noexcept :
                _attributes(&attribute, &attribute + 1),
                _size_in_bytes(attribute.size_in_bytes)
            {}

            constexpr vertex_layout(vertex_layout &&) noexcept = default;

            constexpr vertex_layout & operator = (vertex_layout &&) noexcept = default;

            constexpr bool operator == (const vertex_layout & l) const {
                return this == &l;
            }

            constexpr bool operator != (const vertex_layout & l) const {
                return this != &l;
            }

            constexpr span<const vertex_attribute> attributes() const {
                return _attributes;
            }

            constexpr int32 size_in_bytes() const {
                return _size_in_bytes;
            }

        protected:
            span<const vertex_attribute> _attributes;
            int32 _size_in_bytes;
        };

        namespace vertex_layouts::detail
        {
            template<class T, class ... A> requires(sizeof...(A) > 0)
            constexpr T sum_sizes(meta::types<A...>) {
                return static_cast<T>((A::size_in_bytes + ...));
            }

            template<class T>
            constexpr T sum_sizes(meta::types<>) {
                return 0;
            }

            template <class T, class ... A, size_t ... Indices> requires(sizeof...(Indices) > 0)
            constexpr T sum_string_lengths(std::index_sequence<Indices...>) {
                using attribute_types = meta::types<A...>;
                return static_cast<T>(((sizeof(typename meta::nth_t<Indices, attribute_types>::type) - 1) + ...));
            }

            template <class T, class ... A>
            constexpr T sum_string_lengths(std::index_sequence<>) {
                return 0;
            }

            template <size_t N>
            constexpr void fill(char * dst, const char (&s)[N]) {
                for (size_t i = 0; i < N - 1; ++i, ++dst) {
                    *dst = s[i];
                }
            }

            template <size_t Index, class ... A>
            constexpr void fill(char * dst, const std::tuple<A...> & s) {
                fill(dst + sum_string_lengths<size_t, A...>(std::make_index_sequence<Index>()), get<Index>(s));
            }

            template <size_t ... Indices, class ... A>
            constexpr void fill(char * dst, const std::tuple<A...> & s, std::index_sequence<Indices...>) {
                (fill<Indices>(dst, s), ...);
            }

            template <class T>
            concept cstring_value = requires(T v) {
                requires std::is_array_v<T>;
                same_as<char, decltype(*v)>;
            };

            template <class ... T, size_t DstSize = (sizeof (T) + ...) - sizeof ... (T)>
                requires(cstring_value<T> && ...)
            constexpr std::array<char, DstSize> concat(const T & ... str) {
                std::array<char, DstSize> dest {0};

                fill(dest.data(), std::tuple<const T &...>(str...), std::make_index_sequence<sizeof ... (T)>());

                return dest;
            }
        }

        template <class ... A>
        class static_vertex_layout : public vertex_layout
        {
        public:
            constexpr static_vertex_layout() noexcept :
                vertex_layout(static_vertex_layout::static_attributes, static_vertex_layout::attributes_size_in_bytes)
            {}

            constexpr static_vertex_layout(static_vertex_layout &&) noexcept = default;

            constexpr static_vertex_layout & operator = (static_vertex_layout &&) noexcept = default;

        private:
            using attributes_type = meta::types<A...>;

            static constexpr int32 attributes_count = sizeof ... (A);

            template<size_t N>
            static constexpr int32 slice_sizes = vertex_layouts::detail::sum_sizes<int32>(meta::slice_t<0, N, attributes_type>{});

            template <size_t Index>
            static constexpr vertex_attribute create_attribute() {
                return vertex_attribute::create<typename meta::nth_t<Index, attributes_type>::type>(slice_sizes<Index>);
            }

            template <size_t ... Indices>
            static constexpr std::array<vertex_attribute, attributes_count> create_attributes(std::index_sequence<Indices...>) {
                return {create_attribute<Indices>()...};
            }

            static constexpr std::array<vertex_attribute, attributes_count> static_attributes = create_attributes(std::make_index_sequence<attributes_count>());
            static constexpr int32 attributes_size_in_bytes = vertex_layouts::detail::sum_sizes<int32>(attributes_type{});
        };

        class dynamic_vertex_layout : public vertex_layout
        {
        public:
            using allocator_type = std::pmr::polymorphic_allocator<dynamic_vertex_layout>;

            dynamic_vertex_layout(const allocator_type & alloc = {}) :
                _dynamic_attributes(alloc)
            {}

            dynamic_vertex_layout(dynamic_vertex_layout && l, const allocator_type & alloc = {}) noexcept :
                vertex_layout(std::move(l)),
                _dynamic_attributes{std::move(l._dynamic_attributes), alloc}
            {
                _attributes = _dynamic_attributes;
                l._attributes = {};
            }

            dynamic_vertex_layout & operator = (dynamic_vertex_layout && l) {
                vertex_layout::operator = (std::move(l));
                _dynamic_attributes = std::move(l._dynamic_attributes);
                _attributes = _dynamic_attributes;
                l._attributes = {};

                return *this;
            }

            void append(const vertex_attribute & attribute) {
                _dynamic_attributes.push_back(attribute);
                _attributes = _dynamic_attributes;

                _size_in_bytes += attribute.size_in_bytes;
            }

        private:
            std::pmr::vector<vertex_attribute> _dynamic_attributes;
        };

        namespace vertex_attributes
        {
            template <class Type, int32 Units, bool Normalized = false>
            struct attribute
            {
                using type = Type;

                static constexpr int32 units = Units;
                static constexpr int32 size_in_bytes = Units * sizeof(Type);
                static constexpr bool normalize = Normalized;
            };

            // preset attribute types

            struct p2 : attribute<float, 2>
            {
                static constexpr char key[] = "p2";
                static constexpr char name[] = "position";
            };

            struct p3 : attribute<float, 3>
            {
                static constexpr char key[] = "p3";
                static constexpr char name[] = "position";
            };

            struct p4 : attribute<float, 4>
            {
                static constexpr char key[] = "p4";
                static constexpr char name[] = "position";
            };

            struct n2 : attribute<float, 2>
            {
                static constexpr char key[] = "n2";
                static constexpr char name[] = "normal";
            };

            struct n3 : attribute<float, 3>
            {
                static constexpr char key[] = "n3";
                static constexpr char name[] = "normal";
            };

            struct c3 : attribute<float, 3>
            {
                static constexpr char key[] = "c3";
                static constexpr char name[] = "color";
                static constexpr char alias[] = "color_0";
            };

            struct c4 : attribute<float, 4>
            {
                static constexpr char key[] = "c4";
                static constexpr char name[] = "color";
                static constexpr char alias[] = "color_0";
            };

            struct t2 : attribute<float, 2>
            {
                static constexpr char key[] = "t2";
                static constexpr char name[] = "texcoord";
                static constexpr char alias[] = "texcoord_0";
            };

            struct t3 : attribute<float, 3>
            {
                static constexpr char key[] = "t3";
                static constexpr char name[] = "texcoord";
                static constexpr char alias[] = "texcoord_0";
            };

            struct m : attribute<float, 16>
            {
                static constexpr char key[] = "m";
                static constexpr char name[] = "transform";
            };

            // extended attribute types

            struct c0_3 : attribute<float, 3>
            {
                static constexpr char key[] = "c0_3";
                static constexpr char name[] = "color_0";
                static constexpr char alias[] = "color";
            };

            struct c0_4 : attribute<float, 4>
            {
                static constexpr char key[] = "c0_4";
                static constexpr char name[] = "color_0";
                static constexpr char alias[] = "color";
            };

            struct c1_3 : attribute<float, 3>
            {
                static constexpr char key[] = "c1_3";
                static constexpr char name[] = "color_1";
            };

            struct c1_4 : attribute<float, 4>
            {
                static constexpr char key[] = "c1_4";
                static constexpr char name[] = "color_1";
            };

            struct t0_2 : attribute<float, 2>
            {
                static constexpr char key[] = "t0_2";
                static constexpr char name[] = "texcoord_0";
                static constexpr char alias[] = "texcoord";
            };

            struct t0_3 : attribute<float, 3>
            {
                static constexpr char key[] = "t0_3";
                static constexpr char name[] = "texcoord_0";
                static constexpr char alias[] = "texcoord";
            };

            struct t1_2 : attribute<float, 2>
            {
                static constexpr char key[] = "t1_2";
                static constexpr char name[] = "texcoord_1";
            };

            struct t1_3 : attribute<float, 3>
            {
                static constexpr char key[] = "t1_3";
                static constexpr char name[] = "texcoord_1";
            };
        }

        namespace vertex_layouts
        {
            template <class ... A>
            struct generator
            {
                static constexpr auto key_chars = vertex_layouts::detail::concat(A::key...);
                static constexpr std::string_view key = std::string_view(key_chars.data(), key_chars.size());
                static constexpr static_vertex_layout<A...> instance = {};
            };

            using p2 = generator<vertex_attributes::p2>;
            using p3 = generator<vertex_attributes::p3>;
            using n2 = generator<vertex_attributes::n2>;
            using n3 = generator<vertex_attributes::n3>;
            using c3 = generator<vertex_attributes::c3>;
            using c4 = generator<vertex_attributes::c4>;
            using t2 = generator<vertex_attributes::t2>;
            using t3 = generator<vertex_attributes::t3>;
            using p2n2 = generator<vertex_attributes::p2, vertex_attributes::n2>;
            using p3n3 = generator<vertex_attributes::p3, vertex_attributes::n3>;
            using p2t2 = generator<vertex_attributes::p2, vertex_attributes::t2>;
            using p3t2 = generator<vertex_attributes::p3, vertex_attributes::t2>;
            using p2t3 = generator<vertex_attributes::p2, vertex_attributes::t3>;
            using p3t3 = generator<vertex_attributes::p3, vertex_attributes::t3>;
            using p2c3 = generator<vertex_attributes::p2, vertex_attributes::c3>;
            using p3c3 = generator<vertex_attributes::p3, vertex_attributes::c3>;
            using p2c4 = generator<vertex_attributes::p2, vertex_attributes::c4>;
            using p3c4 = generator<vertex_attributes::p3, vertex_attributes::c4>;
            using p2n2t2 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::t2>;
            using p3n3t2 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::t2>;
            using p2n2t3 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::t3>;
            using p3n3t3 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::t3>;
            using p2n2c3 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::c3>;
            using p3n3c3 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::c3>;
            using p2n2c4 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::c4>;
            using p3n3c4 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::c4>;
            using p2t2c3 = generator<vertex_attributes::p2, vertex_attributes::t2, vertex_attributes::c3>;
            using p3t2c3 = generator<vertex_attributes::p3, vertex_attributes::t2, vertex_attributes::c3>;
            using p2t2c4 = generator<vertex_attributes::p2, vertex_attributes::t2, vertex_attributes::c4>;
            using p3t2c4 = generator<vertex_attributes::p3, vertex_attributes::t2, vertex_attributes::c4>;
            using p2t3c3 = generator<vertex_attributes::p2, vertex_attributes::t3, vertex_attributes::c3>;
            using p3t3c3 = generator<vertex_attributes::p3, vertex_attributes::t3, vertex_attributes::c3>;
            using p2t3c4 = generator<vertex_attributes::p2, vertex_attributes::t3, vertex_attributes::c4>;
            using p3t3c4 = generator<vertex_attributes::p3, vertex_attributes::t3, vertex_attributes::c4>;
            using p2n2t2c3 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::t2, vertex_attributes::c3>;
            using p3n3t2c3 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::t2, vertex_attributes::c3>;
            using p2n2t2c4 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::t2, vertex_attributes::c4>;
            using p3n3t2c4 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::t2, vertex_attributes::c4>;
            using p2n2t3c3 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::t3, vertex_attributes::c3>;
            using p3n3t3c3 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::t3, vertex_attributes::c3>;
            using p2n2t3c4 = generator<vertex_attributes::p2, vertex_attributes::n2, vertex_attributes::t3, vertex_attributes::c4>;
            using p3n3t3c4 = generator<vertex_attributes::p3, vertex_attributes::n3, vertex_attributes::t3, vertex_attributes::c4>;
        }
    }
}
