//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/registry.h>
#include <gfx/texture.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class Gfx>
        struct render_target_buffer_data
        {
            render_target_buffer_data(
                texture_format format = texture_format::rgba,
                texture_filtering filtering = texture_filtering::nearest
            ) :
                format(format),
                filtering(filtering)
            {}

            texture_format format = texture_format::rgba;
            texture_filtering filtering = texture_filtering::nearest;
        };

        template <class Gfx>
        class render_target
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using graphics_type = Gfx;

            void resize(const math::uint_size & size);

            gfx::texture<Gfx> & add_buffer(const render_target_buffer_data<Gfx> & data);
            gfx::texture<Gfx> & get_buffer(int32 index);
        };

        template <class Gfx>
        struct init_data<render_target<Gfx>>
        {
            math::uint_size size;
        };

        template <class Gfx>
        using render_target_data = init_data<render_target<Gfx>>;

        template <class Gfx>
        using render_target_registry = registry<render_target<Gfx>>;
    }
}
