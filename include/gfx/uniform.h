//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/map.h>
#include <gfx/buffer.h>
#include <gfx/uniform_layout.h>

#include <meta/types/nth.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class Gfx>
        class uniform_scheme;

        struct uniform_data
        {
            uniform_data() noexcept = default;

            uniform_data(int id, span<std::byte> chunk) noexcept : id(id), chunk(chunk) {}

            uniform_data(const uniform_data & data) noexcept = default;
            uniform_data(uniform_data && data) noexcept : id(std::exchange(data.id, -1)), chunk(std::move(data.chunk)) {}

            uniform_data & operator = (const uniform_data & data) noexcept = default;
            uniform_data & operator = (uniform_data && data) noexcept {
                std::swap(id, data.id);
                std::swap(chunk, data.chunk);

                return *this;
            }

            int id = -1;
            span<std::byte> chunk;
        };

        template <class Gfx, gfx::buffer_element_type ... T>
        class uniform
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            uniform(uniform_scheme<Gfx> & scheme, uniform_data && data) :
                _scheme(&scheme), _data(std::move(data)) {}

            uniform(uniform && b) noexcept = default;

            inline ~uniform();

            uniform & operator = (uniform && b) noexcept = default;

            template <class T1 = std::tuple<T...>> requires (std::tuple_size<T1>::value == 1)
            uniform & operator = (const meta::first_t<T1> & value) {
                assign(value);
                return *this;
            }

            template <class T1 = std::tuple<T...>, size_t = std::tuple_size<T1>::value>
            uniform & operator = (const T1 & value) {
                iterate_write<0>(value);
                return *this;
            }

            uniform_scheme<Gfx> * scheme() const {
                return _scheme;
            }

            int id() const {
                return _data.id;
            }

            const span<std::byte> & chunk() const {
                return _data.chunk;
            }

            operator const uniform_data & () const {
                return _data;
            }

            inline void activate() const;

            inline void assign(const T &... values) {
                iterate_write<0>(values...);
            }

            template <size_t Index, gfx::buffer_element_type U, class Raw = typename raw_data<plain<U>>::type>
                requires(!math::is_matrix<U>::value)
            inline void assign(const U & value);

            template <size_t Index, class U, class S, class Raw = typename raw_data<math::matrix<U, S>>::type>
            inline void assign(const math::matrix<U, S> & value);

        protected:
            template <size_t Index, class H, class ...U> requires(!Index == sizeof...(T)-1)
            inline void iterate_write(const H & head, const U &... rest) {
                assign<Index>(head);
                iterate_write<Index + 1>(rest...);
            }

            template <size_t Index, class U> requires(Index == sizeof...(T) - 1 && !std::is_same<U, std::tuple<T...>>::value)
            inline void iterate_write(const U & value) {
                assign<Index>(value);
            }

            template <size_t Index> requires(!Index == sizeof...(T)-1)
            inline void iterate_write(const std::tuple<T...> & t) {
                assign<Index>(get<Index>(t));
                iterate_write<Index + 1>(t);
            }

            template <size_t Index> requires(Index == sizeof...(T)-1)
            inline void iterate_write(const std::tuple<T...> & t) {
                assign<Index>(get<Index>(t));
            }

            uniform_scheme<Gfx> * _scheme;
            uniform_data _data;
        };

        template <class Gfx>
        class uniform_scheme
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using graphics_type = Gfx;

            uniform_scheme(const uniform_layout & layout);
            uniform_scheme(uniform_scheme && u) noexcept;

            int id() const;

            const gfx::uniform_layout & layout() const;
            const gfx::uniform_layout_element & at(size_t idx) const;
            const gfx::uniform_layout_element & operator[](size_t idx) const;

            uniform_data instance();
            void select(const uniform_data & uniform);
            void mark_dirty(const uniform_data & uniform);
            void free(const uniform_data & uniform);

            template <gfx::buffer_element_type ... T>
            uniform<Gfx, T...> instance();

            template <gfx::buffer_element_type ... T>
            uniform<Gfx, T...> instance(const T &... args);
        };

        template <class Gfx, gfx::buffer_element_type ... T>
        class uniform_factory
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            uniform_factory(gfx::uniform_scheme<Gfx> & scheme) :
                _scheme(&scheme)
            {}

            uniform<Gfx, T...> create() {
                return _scheme->template instance<T...>();
            }

            uniform<Gfx, T...> create(const T &... args) {
                return _scheme->template instance<T...>(args...);
            }

            operator gfx::uniform_scheme<Gfx> & () {
                return *_scheme;
            }

        private:
            gfx::uniform_scheme<Gfx> * _scheme;
        };

        template <class Gfx>
        class uniform_registry
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            uniform_scheme<Gfx> & scheme(const gfx::uniform_layout & layout);

            template <gfx::buffer_element_type ... T>
            uniform_factory<Gfx, T...> factory(const gfx::static_uniform_layout_storage<T...> & layout);

            template <gfx::buffer_element_type ... T>
            uniform<Gfx, T...> create(const gfx::uniform_layout & layout);

            template <gfx::buffer_element_type ... T>
            uniform<Gfx, T...> create(const gfx::static_uniform_layout_storage<T...> & layout);
        };

        template <class Gfx, gfx::buffer_element_type ... T>
        inline uniform<Gfx, T...>::~uniform() {
            _scheme->free(std::move(_data));
        }

        template <class Gfx, gfx::buffer_element_type ... T>
        inline void uniform<Gfx, T...>::activate() const {
            _scheme->select(_data);
        }

        template <class Gfx, gfx::buffer_element_type ... T>
        template <size_t Index, gfx::buffer_element_type U, class Raw>
            requires(!math::is_matrix<U>::value)
        inline void uniform<Gfx, T...>::assign(const U & value) {
            memcpy(_data.chunk.data() + _scheme->at(Index).offset, &static_cast<const Raw &>(value), sizeof(Raw));
            _scheme->mark_dirty(*this);
        }

        template <class Gfx, gfx::buffer_element_type ... T>
        template <size_t Index, class U, class S, class Raw>
        inline void uniform<Gfx, T...>::assign(const math::matrix<U, S> & value) {
            memcpy(_data.chunk.data() + _scheme->at(Index).offset, Gfx::matrix_order != gfx::matrix_order::row_major ? &static_cast<const Raw &>(math::transpose(value)) : &static_cast<const Raw &>(value), sizeof(Raw));
            _scheme->mark_dirty(*this);
        }
    }
}
