//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/array_list.h>
#include <math/vector.h>
#include <math/matrix.h>
#include <math/rect.h>
#include <gfx/gfx.h>
#include <color/color.h>
#include <ctti/type_id.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class T>
        struct raw_data {};

        template <>
        struct raw_data<float> : identity<float>
        {};

        template <>
        struct raw_data<int> : identity<int>
        {};

        template <class T>
        struct raw_data<color::value<T, color::linear_rgb_format>> : identity<std::array<T, 4>>
        {};

        template <class T>
        struct raw_data<color::value<T, color::srgb_format>> : identity<std::array<T, 4>>
        {};

        template <class T, class S>
        struct raw_data<math::vector<T, S>> : identity<std::array<T, 4>>
        {};

        template <class T, class S>
        struct raw_data<math::matrix<T, S>> : identity<std::array<T, 16>>
        {};

        template <class T, class Model>
        struct raw_data<math::point<T, Model>> : identity<std::array<T, Model::dimensions>>
        {};

        template <class T, class Model>
        struct raw_data<math::size<T, Model>> : identity<std::array<T, Model::dimensions>>
        {};

        template <class T, class Model>
        struct raw_data<math::rect<T, Model>> : identity<std::array<T, 4>>
        {};

        template <class T>
        using raw_data_t = typename raw_data<T>::type;

        template <class T>
        concept buffer_element_type = requires {
            typename raw_data<T>::type;
        };

        constexpr const char * ctti_nameof(ctti::type_tag<asd::f32v2>) {
            return "f32v2";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::f32v3>) {
            return "f32v3";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::f32v4>) {
            return "f32v4";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::f32m2>) {
            return "f32m2";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::f32m3>) {
            return "f32m3";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::f32m4>) {
            return "f32m4";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::i32v2>) {
            return "i32v2";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::i32v3>) {
            return "i32v3";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::i32v4>) {
            return "i32v4";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::i32m2>) {
            return "i32m2";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::i32m3>) {
            return "i32m3";
        }

        constexpr const char * ctti_nameof(ctti::type_tag<asd::i32m4>) {
            return "i32m4";
        }
    }
}
