//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/mesh.h>
#include <gfx/buffer.h>
#include <meta/tuple/unpack.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class Gfx>
        class multimesh_scheme;

        template <class Gfx, class ... Elements>
        class multimesh;

        struct multimesh_data
        {
            size_t pointer;
            size_t size;
        };

        template <class Gfx>
        class multimesh_scheme
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using graphics_type = Gfx;

            multimesh_scheme(const gfx::vertex_layout & layout, const gfx::vertex_layout & instance_layout);

            multimesh_scheme(multimesh_scheme && m) noexcept;

            multimesh_scheme & operator = (multimesh_scheme && m) = delete;

            size_t block_size() const;

            size_t elements_count() const;

            const vertex_attribute & at(size_t idx) const;

            const vertex_attribute & operator[](size_t idx) const;

            void free(const multimesh_data & instance);

            template <class ... Elements>
            multimesh<Gfx, Elements...> instance();

            template <class ... Elements>
            multimesh<Gfx, Elements...> instance(const Elements &... args);

            template <class T>
            void write_attribute(const multimesh_data & instance, ptrdiff_t offset, const T & value);

            template <class T>
            T & read_attribute(const multimesh_data & instance, ptrdiff_t offset);

            void set_indices(const span<const void> & indices, index_type type);

            void set_indices(const span<const u8> & indices) {
                set_indices(span<const void>{indices}, index_type::u8);
            }

            void set_indices(const span<const u16> & indices) {
                set_indices(span<const void>{indices}, index_type::u16);
            }

            void set_indices(const span<const u32> & indices) {
                set_indices(span<const void>{indices}, index_type::u32);
            }

            void render();

            void mark_dirty();
        };

        template <class Gfx, class ... Elements>
        class multimesh
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using element_types = std::tuple<Elements...>;

            multimesh(multimesh_scheme<Gfx> & scheme, const multimesh_data & data) :
                _scheme(&scheme), _data(data) {}

            multimesh(multimesh && b) noexcept : _scheme(b._scheme), _data(std::move(b._data)) {
                b._data.size = 0;
            }

            ~multimesh() {
                clear();
            }

            multimesh & operator = (multimesh && b) noexcept {
                std::swap(_scheme, b._scheme);
                std::swap(_data, b._data);

                return *this;
            }

            template <class Y = meta::first_t<element_types>>
                requires (sizeof ... (Elements) == 1)
            multimesh & operator = (const Y & value) {
                assign(value);
                return *this;
            }

            template <meta::tuple_like Tuple = element_types>
                requires (std::tuple_size_v<Tuple> == sizeof ... (Elements))
            multimesh & operator = (const Tuple & value) {
                meta::unpack(value, [this] (auto & ... values) { assign(values...); });
                return *this;
            }

            size_t pointer() const {
                return _data.pointer;
            }

            operator const multimesh_data & () const {
                return _data;
            }

            template <size_t Index, class ElementType = meta::nth_t<Index, element_types>>
            auto & get() const {
                return _scheme->template read_attribute<ElementType>(_data, _scheme->at(Index).offset);
            }

            void assign(const Elements &... values) {
                meta::apply_indices<sizeof ... (Elements)>([&] <size_t ... Index> (std::index_sequence<Index...>) {
                    (..., write<Index>(values));
                });

                _scheme->mark_dirty();
            }

            template <int Index, buffer_element_type U, class ElementType = meta::nth_t<Index, element_types>>
                requires (std::is_convertible_v<U, ElementType>)
            void assign(const U & value) {
                write<Index>(value);
                _scheme->mark_dirty();
            }

            template <int ... Indices, buffer_element_type ... U>
                requires (sizeof...(Indices) > 1 && sizeof...(Indices) == sizeof...(U))
            void assign(const U &... values) {
                (..., write<Indices>(values));
                _scheme->mark_dirty();
            }

            void clear() {
                if (_data.size > 0) {
                    _scheme->free(_data);
                    _data = {0, 0};
                }
            }

        protected:
            template <int Index, buffer_element_type U, class ElementType = meta::nth_t<Index, element_types>>
                requires (std::is_convertible_v<U, ElementType>)
            void write(const U & value) {
                using raw_type = raw_data_t<ElementType>;

                if constexpr (math::is_matrix_v<U>) {
                    _scheme->write_attribute(_data, _scheme->at(Index).offset, Gfx::matrix_order != gfx::matrix_order::row_major ? static_cast<const raw_type &>(math::transpose(value)) : static_cast<const raw_type &>(value));
                } else {
                    _scheme->write_attribute(_data, _scheme->at(Index).offset, static_cast<const raw_type &>(value));
                }
            }

            multimesh_scheme<Gfx> * _scheme;
            multimesh_data _data;
        };

        template <class Gfx>
        struct init_data<multimesh_scheme<Gfx>>
        {
            const gfx::vertex_layout & layout;
            const gfx::vertex_layout & instance_layout;
            gfx::primitives_type primitives_type = gfx::primitives_type::triangles;
            gfx::polygon_face visible_face = gfx::polygon_face::front;
        };

        template <class Gfx>
        using multimesh_scheme_data = init_data<multimesh_scheme<Gfx>>;

        template <class Gfx>
        using multimesh_registry = registry<multimesh_scheme<Gfx>>;
    }
}
