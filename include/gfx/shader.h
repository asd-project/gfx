//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/span.h>

#include <gfx/registry.h>
#include <gfx/vertex_layout.h>
#include <gfx/uniform_layout.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        struct shader_code_unit
        {
            const char * const code;
            uint32 type;
        };

        struct shader_code
        {
            template <size_t N>
            constexpr shader_code(const shader_code_unit (& units)[N], const vertex_layout & layout, span<const std::string_view> sampler_slots) noexcept :
                units(units),
                layout(&layout),
                sampler_slots(sampler_slots)
            {}

            constexpr shader_code(shader_code && code) noexcept = default;
            constexpr shader_code & operator = (shader_code && code) noexcept = default;

            span<const shader_code_unit> units;
            const vertex_layout * layout;
            span<const std::string_view> sampler_slots;
        };

        template <class Gfx>
        class shader_program
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using graphics_type = Gfx;

            const vertex_layout & layout() const;

            void activate();
        };

        template <class Gfx>
        struct init_data<shader_program<Gfx>>
        {
            std::string_view name;
            const gfx::shader_code & code;
        };

        template <class Gfx>
        using shader_program_data = init_data<shader_program<Gfx>>;

        template <class Gfx>
        class registry<shader_program<Gfx>>
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            gfx::handle<gfx::shader_program<Gfx>> create(const shader_program_data<Gfx> & data);
        };

        template <class Gfx>
        using shader_registry = registry<shader_program<Gfx>>;
    }
}
