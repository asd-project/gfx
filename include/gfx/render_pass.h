//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <optional>

#include <meta/flag.h>

#include <color/color.h>

#include <gfx/registry.h>
#include <gfx/render_target.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        enum class feature : u8
        {
            depth_test,
            stencil_test
        };

        adapt_enum_flags(feature);

        using features = enum_flags<gfx::feature>;

        enum class blending
        {
            none,
            interpolative,
            additive,
            multiplicative
        };

        template <class Gfx>
        class render_pass
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            using graphics_type = Gfx;

            void start();
            void end();
        };

        template <class Gfx>
        struct init_data<render_pass<Gfx>>
        {
            init_data() {}

            init_data(
                gfx::features features,
                gfx::blending blending = gfx::blending::none,
                const color::linear_rgb & clear_color = {0.0f, 0.0f, 0.0f},
                float clear_depth = 1.0f,
                float clear_stencil = 0.0f
            ) :
                features(features),
                blending(blending),
                clear_color(clear_color),
                clear_depth(clear_depth),
                clear_stencil(clear_stencil)
            {}

            init_data(
                gfx::render_target<Gfx> & target,
                gfx::features features,
                gfx::blending blending = gfx::blending::none,
                const color::linear_rgb & clear_color = {0.0f, 0.0f, 0.0f},
                float clear_depth = 1.0f,
                float clear_stencil = 0.0f
            ) :
                target(&target),
                features(features),
                blending(blending),
                clear_color(clear_color),
                clear_depth(clear_depth),
                clear_stencil(clear_stencil)
            {}

            gfx::render_target<Gfx> * target = nullptr;
            gfx::features features;
            gfx::blending blending = gfx::blending::none;
            color::linear_rgb clear_color = {0.0f, 0.0f, 0.0f};
            float clear_depth = 1.0f;
            float clear_stencil = 0.0f;
        };

        template <class Gfx>
        using render_pass_data = init_data<render_pass<Gfx>>;

        template <class Gfx>
        using render_pass_registry = registry<render_pass<Gfx>>;
    }
}
