//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <memory>

#include <boost/pool/object_pool.hpp>

#include <gfx/gfx.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <gfx::object Object>
        struct init_data {};

        struct pool_object_deleter
        {
            template<gfx::object Object>
            void operator()(Object * object) noexcept {
                static_cast<boost::object_pool<Object> *>(pool)->destroy(object);
            }

            void * pool = nullptr;
        };

        template <gfx::object Object>
        using handle = std::unique_ptr<Object, pool_object_deleter>;

        template <class Impl, gfx::object Object>
        class basic_registry
        {
        public:
            using object_type = Object;
            using graphics_type = typename Object::graphics_type;

            gfx::handle<Object> create(init_data<Object> && data) {
                return {
                    static_cast<Impl *>(this)->construct(_pool.malloc(), std::move(data)),
                    pool_object_deleter{&_pool}
                };
            }

        private:
            boost::object_pool<Object> _pool;
        };

        template <gfx::object Object>
        class registry : public basic_registry<registry<Object>, Object>
        {
        public:
            auto construct(void * location, init_data<Object> && data) {
                return new (location) Object(std::move(data));
            }
        };
    }
}
