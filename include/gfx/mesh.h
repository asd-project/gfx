//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/registry.h>

#include <gfx/vertex_layout.h>
#include <gfx/vertex_data.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        enum class primitives_type
        {
            triangles,
            lines,
            triangle_strip,
            line_strip
        };

        enum class mesh_type
        {
            plain,
            indexed
        };

        template <class Gfx>
        class vertex_buffer
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            void update(const vertex_data & vd) = delete;
            void update_region(const math::uint_range & region, const vertex_data & vd) = delete;

            template <class T> requires (!std::is_same_v<plain<T>, void>)
            void update_region(const math::uint_range & region, const span<T> & vd) {
                update_region(region * sizeof(T), vertex_data(vd));
            }
        };

        enum class index_type
        {
            u8,
            u16,
            u32
        };

        enum class polygon_face
        {
            none,
            front,
            back,
            both
        };

        template <class Gfx>
        class mesh
        {
        public:
            using graphics_type = Gfx;

            i32 vertices_count() const;
            i32 primitives_count() const;

            vertex_buffer<Gfx> & add_vertex_buffer(const vertex_layout & layout, const vertex_data & data = {});

            void set_indices(const span<const void> & indices, index_type type);

            void set_indices(const span<const u8> & indices) {
                set_indices(span<const void>{indices}, index_type::u8);
            }

            void set_indices(const span<const u16> & indices) {
                set_indices(span<const void>{indices}, index_type::u16);
            }

            void set_indices(const span<const u32> & indices) {
                set_indices(span<const void>{indices}, index_type::u32);
            }

            void render();
        };

        template <class Gfx>
        struct init_data<gfx::mesh<Gfx>>
        {
            const gfx::vertex_layout & layout;
            primitives_type primitives_type = primitives_type::triangles;
            polygon_face visible_face = polygon_face::front;
        };

        template <class Gfx>
        using mesh_data = init_data<mesh<Gfx>>;

        template <class Gfx>
        using mesh_registry = registry<mesh<Gfx>>;

        inline int primitive_indices_count(gfx::primitives_type primitives_type) {
            switch (primitives_type) {
                case gfx::primitives_type::triangles:
                    return 3;

                case gfx::primitives_type::lines:
                    return 2;

                case gfx::primitives_type::triangle_strip: [[ fallthrough ]];
                case gfx::primitives_type::line_strip:
                    return 1;
            }

            BOOST_ASSERT_MSG(false, "[gfx][primitive_indices_count] Invalid primitives type");
            return 1;
        }
    }
}
